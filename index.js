// console.log("Hello World");


let number = Number(prompt("Give me a number"));
console.log("The number you provided is " + number + ".");

for (let count = number; count >= 0; count--) {

 	if (count <= 50) {

		console.log("The current value is at " + count + ". Terminating the loop.");
		break;

	}
	if (count % 10 === 0) {

		console.log("The number is divisible by 10. Skipping the number.");
		continue;

	}
	if (count % 5 == 0) { 

        console.log(count);

    }
}

let string = 'supercalifragilisticexpialidocious';
let filteredString = '';


for (let i=0; i < string.length; i++) {


	if (
		string[i].toLowerCase() == 'a' ||
		string[i].toLowerCase() == 'e' ||
		string[i].toLowerCase() == 'i' ||
		string[i].toLowerCase() == 'o' ||
		string[i].toLowerCase() == 'u'
	) {

	
		continue;

	
	} else {

	
		filteredString += string[i];

	}
}

console.log(string);
console.log(filteredString);
